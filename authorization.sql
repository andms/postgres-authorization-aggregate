CREATE EXTENSION "uuid-ossp";

CREATE TABLE "authorization_key_store" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v1mc(),
  "pub_key" bytea,
  "username" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "prev_id" uuid,
  "next_id" uuid
);
ALTER TABLE "authorization_key_store" OWNER TO "postgres";

CREATE TABLE "account" (
  "id" serial,
  "username" varchar(255) COLLATE "pg_catalog"."default" NOT NULL UNIQUE,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL UNIQUE,
  "password" char(60) COLLATE "pg_catalog"."default" NOT NULL,
  "is_account_locked" boolean NOT NULL DEFAULT FALSE,
  "is_account_disabled" boolean NOT NULL DEFAULT FALSE,
  CONSTRAINT "account_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "account" OWNER TO "postgres";

CREATE TABLE "password_reset" (
  "id" serial,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL UNIQUE,
  "token" char(60) COLLATE "pg_catalog"."default" NOT NULL,
  "create_timestamp" timestamp NOT NULL,
  CONSTRAINT "password_reset_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "password_reset" OWNER TO "postgres";
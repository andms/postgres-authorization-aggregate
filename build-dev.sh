#!/bin/bash

docker-compose down -v

docker login registry.gitlab.com
docker build -t registry.gitlab.com/andms/postgres-authorization-aggregate .
docker push registry.gitlab.com/andms/postgres-authorization-aggregate

docker-compose up -d

sleep 5

docker cp accounts.csv postgres-authorization-aggregate:/tmp/accounts.csv

docker exec -it postgres-authorization-aggregate psql -U 'postgres' -d 'postgres' -c "\COPY account(username,email,password) FROM '/tmp/accounts.csv' DELIMITER ',' CSV HEADER;"